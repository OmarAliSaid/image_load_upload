package com.appz.imagetask;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by OmarAli on 27/09/2015.
 */
public class UploadImage extends AsyncTask<String, Void, Boolean> {

    String link;
    @Override
    protected void onPreExecute() {
        // show the progress bar
    }

    @Override
    protected Boolean doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL(Constants.Server);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", "Client-ID "+Constants.Client_ID);

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("image", params[0])
                    .appendQueryParameter("key", Constants.API_key);
            String query = builder.build().getEncodedQuery();

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();
            String response="";
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                Log.d("response : ", response);
                // get the returned link from the response
                JSONObject jsonObject = new JSONObject(response);
                link=jsonObject.getJSONObject("data").getString("link");
                Log.d("link : ", link);
            }
            conn.connect();
            return true;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean parsingError) {
        // hide the progress bar
    }
}