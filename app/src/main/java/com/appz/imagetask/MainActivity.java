package com.appz.imagetask;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements Button.OnClickListener{

    private static int RESULT_LOAD_IMG = 1;
    private static final int CAMERA_REQUEST = 1888;
    Button upload;
    Button load;
    ImageView iv_upload;
    ImageView iv_load;
    Bitmap photoPic;
    ProgressBar progressBar;
    String link;
    ImageLoaderConfiguration config;

    public void initialize(){
        upload=(Button)findViewById(R.id.btn_upload);
        load=(Button)findViewById(R.id.btn_load);
        iv_upload=(ImageView)findViewById(R.id.iv_upload);
        iv_load=(ImageView)findViewById(R.id.iv_load);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        upload.setOnClickListener(this);
        load.setOnClickListener(this);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        // set the Universal Image Loader ImageOptions to cache images
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();
        //build ImageLoader configuration with the set ImageOptions
        config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_upload:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("pick image");
                builder.setPositiveButton("gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // start intent for picking image from gallery on gallery button clicked
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMG);
                    }
                });
                builder.setNegativeButton("camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // start intent for picking image from camera on camera button clicked
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                break;

            case R.id.btn_load:

                //check if upload image view has an image to avoid error if user clicked load button first
                if(iv_upload.getDrawable() != null){
                    // check if image is cached or not
                    if(isCached(link).equals("")){
                        // if image not cached then load it from the internet with the link

                        //check for internet connection before start loading image
                        if(isNetworkAvailable()==true){
                            ImageLoader.getInstance().loadImage(link, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(MainActivity.this, "Error Loading image .. please try again later", Toast.LENGTH_LONG)
                                            .show();
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar.setVisibility(View.GONE);
                                    iv_load.setImageBitmap(loadedImage);
                                    Toast.makeText(MainActivity.this, "image loaded from internet", Toast.LENGTH_LONG)
                                            .show();
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        }else{
                            Toast.makeText(MainActivity.this, "Please check internet connection", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                    else{
                        // load image from cache and set it to load image view
                        iv_load.setImageURI(Uri.parse(isCached(link)));
                        Toast.makeText(MainActivity.this, "image loaded from cache", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                // handling case if user clicked load before upload
                else{
                    Toast.makeText(MainActivity.this, "Please Upload Image First", Toast.LENGTH_LONG)
                            .show();
                }
                break;
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Boolean isPhotoPicked=false; // boolean to check if user picked image from gallery or camera
        try {
            // handling case user opens gallery or camera and doesn't pick image
            if(resultCode != RESULT_CANCELED){

                // ************ When an Image is picked from gallery *****************
                if (requestCode == RESULT_LOAD_IMG && resultCode == this.RESULT_OK && null != data) {

                isPhotoPicked=true;
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                // Set the Bitmap photoPic to the image picked from the gallery
                photoPic = BitmapFactory.decodeFile(imgDecodableString);

            }
                // ************ When an Image is picked from camera *****************
            else
                if(requestCode == CAMERA_REQUEST && resultCode == this.RESULT_OK ) {
                    isPhotoPicked=true;
                    // Set the Bitmap photoPic to the image picked from the camera
                    photoPic = (Bitmap) data.getExtras().get("data");
            }

        }} catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
        // if user already picked an image then convert it to String byte array and call UploadImage Thread to upload the image
        if(isPhotoPicked==true){
            // converting photoPic to String byte array
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photoPic.compress(Bitmap.CompressFormat.PNG, 50, stream);
            byte[] byte_arr = stream.toByteArray();
            // if internet connection available then upload the image
            if(isNetworkAvailable()){
                new UploadImage().execute(Base64.encodeToString(byte_arr, 0));
            }else{
                Toast.makeText(MainActivity.this, "Please check Internet connection", Toast.LENGTH_LONG)
                        .show();
            }
        }

    }

    public class UploadImage extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // show the progress bar
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {

            URL url = null;
            try {
                url = new URL(Constants.Server);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Authorization", "Client-ID "+Constants.Client_ID);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("image", params[0])
                        .appendQueryParameter("key", Constants.API_key);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();
                String response="";
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                    // get the returned link from the response
                    Log.d("response : ", response);
                    JSONObject jsonObject = new JSONObject(response);
                    link=jsonObject.getJSONObject("data").getString("link");
                    Log.d("link : ", link);
                    Log.d("hopa : ", "hhhhhh");
                }
                conn.connect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean parsingError) {
            // hide the progress bar
            progressBar.setVisibility(View.GONE);
            iv_upload.setImageBitmap(photoPic);
            Toast.makeText(MainActivity.this, "Image Uploaded successfully", Toast.LENGTH_LONG)
                    .show();
        }
    }

    // function for checking internet connection
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // take the image url and check if it is cached or not
    public String isCached(String imageURL){
        File file = DiskCacheUtils.findInCache(imageURL, ImageLoader.getInstance().getDiskCache());
        if (file==null) {
            return "";
        }
        else {
            return (file.getAbsolutePath() );
        }
    }

}
